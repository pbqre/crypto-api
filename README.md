## README

### Some resources:
- https://www.django-rest-framework.org/api-guide/authentication/#remoteuserauthentication 
- https://django-rest-framework-simplejwt.readthedocs.io/en/latest/
- https://james1345.github.io/django-rest-knox/
- https://2factor.in/
- https://www.twilio.com/
- https://pyauth.github.io/pyotp/#quick-overview-of-using-one-time-passwords-on-your-phone
