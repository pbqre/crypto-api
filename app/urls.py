from django.urls import path
from app import views

urlpatterns = [
  path('', views.home, name='home'),
  path('ticker', views.ticker, name='ticker'),
  path('todo_list', views.todo_list, name='todo_list'),
  path('todo_detail/<int:pk>', views.todo_detail, name='todo_detail'),
  path('todo_class_detail/<int:pk>', views.TodoDetail.as_view(), name='todo_class_detail'),
]