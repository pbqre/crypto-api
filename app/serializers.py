from rest_framework import serializers
from app.models import Todo


class TodoSerializer(serializers.Serializer):
  task = serializers.CharField()
  time = serializers.DateTimeField(required=False)
  is_completed = serializers.BooleanField(required=False)

  def create(self, validated_data):
    return Todo.objects.create(**validated_data)
  
  def update(self, instance, validated_data):
      instance.task = validated_data.get('task', instance.task)
      instance.time = validated_data.get('time', instance.time)
      instance.is_completed = validated_data.get('is_completed', instance.is_completed)
      instance.save()
      return instance
  