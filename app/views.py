from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.models import User
from app import serializers
from app.models import Todo
from app.serializers import TodoSerializer
import requests
from django.http import Http404

# Rest Framework imports.
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

class TodoDetail(APIView):
  authentication_classes = [TokenAuthentication]
  permission_classes = [IsAuthenticated, ]
  def get_object(self, pk):
    try:
      todo = Todo.objects.get(pk=pk)
      return todo
    except Todo.DoesNotExist:
      # return Response({'msg': 'The todo you\'re requesting does not exist.'}, status=404)
      raise Http404

  def get(self, request, pk):
    todo = self.get_object(pk)
    serializer = TodoSerializer(todo)
    return Response(serializer.data)
  
  def put(self, request, pk):
    todo = self.get_object(pk)
    serializer = TodoSerializer(instance=todo, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    else:
      return Response(serializer.errors, status=400)
  
  def delete(self, request, pk):
    todo = self.get_object(pk)
    todo.delete()
    return Response({'msg':'Deleted'}, status=204)



@api_view(['GET', 'PUT', 'DELETE'])
def todo_detail(request, pk):
  """
  Requirements:
    - It'll return that specific todo.
    - It can update a todo.
    - It can delete a todo.
  """
  try:
    todo = Todo.objects.get(pk=pk)
  except Todo.DoesNotExist:
    return Response({'msg': 'The todo you\'re requesting does not exist.'}, status=404)
  
  if request.method == 'GET':
    serializer = TodoSerializer(todo)
    return Response(serializer.data)
  
  elif request.method == 'PUT':
    serializer = TodoSerializer(instance=todo, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    else:
      return Response(serializer.errors, status=400)
  
  elif request.method == 'DELETE':
    todo.delete()
    return Response({'msg':'Deleted'}, status=204)
    

  


@api_view(['GET', 'POST'])
def todo_list(request):
  if request.method == 'GET':
    todos = Todo.objects.all()
    serializer = TodoSerializer(todos, many=True)
    return Response(serializer.data)
  
  elif request.method == 'POST':
    serializer = TodoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=201)
    else:
      return Response(serializer.errors, status=400)



@api_view(['GET', 'POST'])
def home(request):
  users = User.objects.all()
  data = []

  for user in users:
    data.append(
      {
        'username': user.username,
        'email': user.email
      }
    )
  return Response(data)

def ticker(request):
  r = requests.get('https://api.wazirx.com/api/v2/tickers')
  
  data = r.json()

  return JsonResponse(data, safe=False)  